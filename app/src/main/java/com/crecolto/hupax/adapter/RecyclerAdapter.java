package com.crecolto.hupax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.crecolto.hupax.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private HashMap<Integer, JSONObject> mData = null ;
    private String beforeData;

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView date_info, time, nick_name, title ;
        LinearLayout divider;

        ViewHolder(View itemView) {
            super(itemView) ;

            date_info = itemView.findViewById(R.id.date_info) ;
            time = itemView.findViewById(R.id.time) ;
            nick_name = itemView.findViewById(R.id.nick_name) ;
            title = itemView.findViewById(R.id.title) ;
            divider = itemView.findViewById(R.id.divider);
        }
    }

    public RecyclerAdapter(HashMap<Integer, JSONObject> list) {
        mData = list ;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext() ;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;

        View view = inflater.inflate(R.layout.item_recycler, parent, false) ;
        RecyclerAdapter.ViewHolder vh = new RecyclerAdapter.ViewHolder(view) ;

        return vh ;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {

        try {

            if(mData.get(position).getString("view_type").equals("has_header")){

                holder.divider.setVisibility(View.VISIBLE);
                holder.date_info.setText(mData.get(position).getString("date") + "(" + mData.get(position).getString("day") + ")") ;
                beforeData = mData.get(position).getString("date");

            }else{

                holder.divider.setVisibility(View.GONE);

            }

            holder.time.setText(mData.get(position).getString("time")) ;
            holder.nick_name.setText(mData.get(position).getString("type")) ;
            holder.title.setText(mData.get(position).getString("nickname")) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mData.size() ;
    }
}
