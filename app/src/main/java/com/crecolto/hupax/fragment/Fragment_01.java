package com.crecolto.hupax.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crecolto.hupax.R;
import com.crecolto.hupax.adapter.RecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class Fragment_01 extends Fragment {

    TextView ui_date, ui_day, ui_year;
    HashMap<Integer, JSONObject> tempData;
    RecyclerView rv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_01, container, false);

        ui_date = (TextView) rootView.findViewById(R.id.ui_date);
        ui_day = (TextView) rootView.findViewById(R.id.ui_day);
        ui_year = (TextView) rootView.findViewById(R.id.ui_year);

        rv = (RecyclerView) rootView.findViewById(R.id.rv);

        SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd");

        String format_time1 = format1.format (System.currentTimeMillis());

        String[] dateData = format_time1.split("-");

        ui_date.setText(getActivity().getResources().getString(R.string.item_resource_date, dateData[1], dateData[2]));
        ui_year.setText(getActivity().getResources().getString(R.string.item_resource_year, dateData[0]));

        Calendar cal= Calendar.getInstance ( );

        int day_of_week = cal.get ( Calendar.DAY_OF_WEEK );

        if ( day_of_week == 1 )
            ui_day.setText("(일)");
        else if ( day_of_week == 2 )
            ui_day.setText("(월)");
        else if ( day_of_week == 3 )
            ui_day.setText("(화)");
        else if ( day_of_week == 4 )
            ui_day.setText("(수)");
        else if ( day_of_week == 5 )
            ui_day.setText("(목)");
        else if ( day_of_week == 6 )
            ui_day.setText("(금)");
        else if ( day_of_week == 7 )
            ui_day.setText("(토)");

        dataSet();

        RecyclerView recyclerView = rootView.findViewById(R.id.rv) ;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity())) ;

        RecyclerAdapter adapter = new RecyclerAdapter(tempData) ;
        recyclerView.setAdapter(adapter) ;

        return rootView;

    }

    private void dataSet(){

        tempData = new HashMap<>();

        JSONArray jsonArray = new JSONArray();
        JSONObject jOb = new JSONObject();

        try {

            jOb.put("nickname", "주일 새벽미사");
            jOb.put("date", "2013-09-22");
            jOb.put("type", "주일미사");
            jOb.put("time", "06:30");
            jOb.put("day", "주일");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "주일 오전미사");
            jOb.put("date", "2013-09-22");
            jOb.put("type", "주일미사");
            jOb.put("time", "08:30");
            jOb.put("day", "주일");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "주일 교중미사");
            jOb.put("date", "2013-09-22");
            jOb.put("type", "주일미사");
            jOb.put("time", "10:30");
            jOb.put("day", "주일");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "주일 저녁미사");
            jOb.put("date", "2013-09-22");
            jOb.put("type", "주일미사");
            jOb.put("time", "17:30");
            jOb.put("day", "주일");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "월요 새벽미사");
            jOb.put("date", "2013-09-23");
            jOb.put("type", "평일미사");
            jOb.put("time", "06:30");
            jOb.put("day", "월");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "화요 오전미사");
            jOb.put("date", "2013-09-24");
            jOb.put("type", "평일미사");
            jOb.put("time", "09:30");
            jOb.put("day", "화");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "화요 저녁미사");
            jOb.put("date", "2013-09-24");
            jOb.put("type", "평일미사");
            jOb.put("time", "19:30");
            jOb.put("day", "화");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "수요 오전미사");
            jOb.put("date", "2013-09-25");
            jOb.put("type", "평일미사");
            jOb.put("time", "10:00");
            jOb.put("day", "수");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "수요 저녁미사");
            jOb.put("date", "2013-09-25");
            jOb.put("type", "평일미사");
            jOb.put("time", "19:30");
            jOb.put("day", "수");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "목요 오전미사");
            jOb.put("date", "2013-09-26");
            jOb.put("type", "평일미사");
            jOb.put("time", "10:00");
            jOb.put("day", "목");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "목요 저녁미사");
            jOb.put("date", "2013-09-26");
            jOb.put("type", "평일미사");
            jOb.put("time", "19:30");
            jOb.put("day", "목");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "금요 오전미사");
            jOb.put("date", "2013-09-27");
            jOb.put("type", "평일미사");
            jOb.put("time", "10:00");
            jOb.put("day", "금");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "토요 오전미사");
            jOb.put("date", "2013-09-28");
            jOb.put("type", "평일미사");
            jOb.put("time", "10:00");
            jOb.put("day", "토");
            jOb.put("view_type", "has_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "주일학교 초등부 미사");
            jOb.put("date", "2013-09-28");
            jOb.put("type", "특별미사");
            jOb.put("time", "16:00");
            jOb.put("day", "토");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);
            jOb = new JSONObject();

            jOb.put("nickname", "토요 저녁 주일미사");
            jOb.put("date", "2013-09-28");
            jOb.put("type", "토요일 저녁 주일미사");
            jOb.put("time", "18:00");
            jOb.put("day", "토");
            jOb.put("view_type", "none_header");

            jsonArray.put(jOb);

            for(int i = 0; i < jsonArray.length(); i++){

                tempData.put(i, jsonArray.getJSONObject(i));

            }

            Log.d("SET_DATA", "DATA : " + tempData.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}

