package com.crecolto.hupax;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog pd = null;

    public void toastMaker(String str){

        Toast.makeText(this, str, Toast.LENGTH_SHORT ).show();

    }

    public void showProgress(){

        if(pd == null){

            pd = new ProgressDialog(BaseActivity.this, R.style.MyTheme);
                pd.setCancelable(true);
                pd.setProgressStyle(android.R.style.Widget_ProgressBar_Large_Inverse);
            Drawable drawable = new ProgressBar(this).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            pd.setIndeterminateDrawable(drawable);

        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd.show();
            }
        });

    }

    public void hideProgress(){

        if(pd != null && pd.isShowing()){

            pd.dismiss();

        }

    }

}
