package com.crecolto.hupax.utils;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class CipherUtils {

    //키 지정(마스터키 용)
    private String cipherPass = "";

    //키를 Hex 방식으로 리턴
    private String sk = "";
    private Key key = null;
    private Cipher cipher = null;

    //알고리즘/모드/패딩방식 지정
    String transformation = "AES/ECB/PKCS5Padding";

    public CipherUtils(String password) throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, InvalidKeySpecException, IllegalArgumentException, NoSuchPaddingException {
        cipherPass=password;
        sk = ByteUtility.toHexString(this.cipherPass.getBytes());
        key = generateKey("AES", ByteUtility.toBytes(sk, 16));
        cipher = Cipher.getInstance(transformation);
    }

    /**
     * 해당 알고리즘으로 암호화하여 Base64 인코딩한 후 리턴한다.
     * @return Encrypt Encoding String
     */
    public String encrypt(String encryptStr)throws Exception{
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] plain = encryptStr.getBytes("UTF-8");
        byte[] encrypt = cipher.doFinal(plain);
        return new String(Base64Coder.encode(encrypt));
    }


    /**
     * 주어진 데이터로, 해당 알고리즘에 사용할 비밀키(SecretKey)를 생성한다.
     * @param algorithm DES/DESede/TripleDES/AES
     * @param keyData
     */
    public Key generateKey(String algorithm, byte[] keyData) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        String upper = algorithm.toUpperCase();
        if ("DES".equals(upper)) {
            KeySpec keySpec = new DESKeySpec(keyData);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
            return secretKey;
        } else if ("DESede".equals(upper) || "TripleDES".equals(upper)) {
            KeySpec keySpec = new DESedeKeySpec(keyData);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
            return secretKey;
        } else {
            SecretKeySpec keySpec = new SecretKeySpec(keyData, algorithm);
            return keySpec;
        }
    }

}
