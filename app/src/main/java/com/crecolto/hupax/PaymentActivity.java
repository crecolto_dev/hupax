package com.crecolto.hupax;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import com.crecolto.hupax.utils.AppUtil;
import com.crecolto.hupax.utils.CipherUtils;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentActivity extends BaseActivity implements View.OnClickListener {

    private TextView pm_tv, user_name, btn_cancel, btn_reset;
    private int type, p_type;
    private Button btn_pm_qr;
    private EditText input_price;
    private String partnerId = "KIS1234567890";  // 임의값 변경 필요 -> 가맹점의 고객별 ID (30 bytes 초과시 오류 발생)
    private String partnerCd = "BAA91";          // 전달된 가맹점(매장)별 파트너 코드 사용 테스트용 : BAA91
    private String merchantCd = "1168143939";    // 파트너코드 발급 시 등록한 사업자번호 사용 테스트용 : 116-81-43939
    private final String vanId = "000002";
    private String otc;
    private String price = "1004";   // 결제 금액
    private String authNum;
    private String authDate;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        pm_tv = (TextView) findViewById(R.id.pm_tv);
        btn_pm_qr = (Button) findViewById(R.id.btn_pm_qr);
        input_price = (EditText) findViewById(R.id.input_price);
        user_name = (TextView) findViewById(R.id.user_name);
        btn_cancel = (TextView) findViewById(R.id.btn_cancel);
        btn_reset = (TextView) findViewById(R.id.btn_reset);
        btn_pm_qr.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_reset.setOnClickListener(this);

        Intent intent = getIntent();
        type = (int) intent.getSerializableExtra("type");
        p_type = (int) intent.getSerializableExtra("payment_type");
        user_name.setText(String.format(getResources().getString(R.string.item_user_name), AppUtil.getUserName()));

        setElement(type, p_type);

    }

    private void setElement(int type, int p_type){

        /**
         * p_type
         * 0 : credit card
         * 1 : QR code
         * 2 : nfc
         */

        switch (p_type){

            case 0 : pm_tv.setText(getResources().getString(R.string.item_pm_kind_00));break;

            case 1 : pm_tv.setText(getResources().getString(R.string.item_pm_kind_01));break;

            case 2 : pm_tv.setText(getResources().getString(R.string.item_pm_kind_02));break;

        }

    }

    private final static String APPCARD_PKG = "com.nice.appcard";

    private boolean isInstalledApplication() {

        PackageManager pm = getPackageManager();

        try {
            pm.getApplicationInfo(APPCARD_PKG, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static String getH(String parameters, String key) throws Exception {
        CipherUtils cu = new CipherUtils(key);
        String b = URLEncoder.encode(cu.encrypt(parameters), "UTF-8");
        return b;
    }

    public static String getSystemDate() {
        long time = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hhmmss");
        Date dd = new Date(time);
        return sdf.format(dd);
    }


    public static String getParamHash256(String password){
        MessageDigest digest;
        StringBuffer sb = new StringBuffer();
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());
            byte[] input = digest.digest();
            for(int i = 0 ; i < input.length ; i++){
                sb.append(Integer.toString((input[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private final String PAYMENT_LOG = "RESULT_INFO";

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {

        if(requestCode == 0){

            Log.d(PAYMENT_LOG, "request code : " + String.valueOf(requestCode));

            if(resultCode == RESULT_OK) {

                Log.i(PAYMENT_LOG, "resultType : " + data.getStringExtra("resultType"));
                Log.i(PAYMENT_LOG, "resultCode : " + resultCode);
                Log.i(PAYMENT_LOG, "result all data : " + data.toUri(Intent.URI_INTENT_SCHEME));
                Log.i(PAYMENT_LOG, "resultOTC : " + data.getStringExtra("OTC"));
                Log.i(PAYMENT_LOG, "resultID_CD : " + data.getStringExtra("ID_CD"));

                moveToPaymentResult();

                /*StringBuilder sb = new StringBuilder();
                otc = data.getStringExtra("OTC"); // 승인전문에 필요한 OTC*/

            }else{

                rsDialog(resultCode);

            }

        }else {

            super.onActivityResult(requestCode, resultCode, data);

        }

    }

    private void rsDialog(int resultCode){

        String rsStr = "";

        switch (resultCode){

            case 0 : rsStr = getResources().getString(R.string.item_payment_appcard_0);break;

            case 400 : rsStr = getResources().getString(R.string.item_payment_appcard_400);break;

            case 405 : rsStr = getResources().getString(R.string.item_payment_appcard_405);break;

            case 801 : rsStr = getResources().getString(R.string.item_payment_appcard_801);break;

            case 802 : rsStr = getResources().getString(R.string.item_payment_appcard_802);break;

            case 803 : rsStr = getResources().getString(R.string.item_payment_appcard_803);break;

            case 812 : rsStr = getResources().getString(R.string.item_payment_appcard_812);break;

            case 813 : rsStr = getResources().getString(R.string.item_payment_appcard_813);break;

            case 815 : rsStr = getResources().getString(R.string.item_payment_appcard_815);break;

            case 201 : rsStr = getResources().getString(R.string.item_payment_appcard_201);break;

            case 202 : rsStr = getResources().getString(R.string.item_payment_appcard_202);break;

            default :  rsStr = getResources().getString(R.string.item_payment_appcard_default);

        }

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.item_payment_appcard_title))
                .setMessage(rsStr)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }

    private void moveToPaymentResult(){

        Intent intent = new Intent(PaymentActivity.this, PaymentResultActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("payment_type", p_type);
        intent.putExtra("price", Integer.parseInt(price));
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

    }

    private void goToPaymentByCard(){

        price = input_price.getText().toString();

        if(price.equals("")){

            toastMaker("헌금 금액을 입력하세요");
            return;

        }

        int temp_price = Integer.parseInt(price);

        if(temp_price < 9999 /*&& temp_price < 500001 && temp_price % 10000 == 0*/){

            if(isInstalledApplication()){

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                String systemTime = getSystemDate();
                String hPartnerId = partnerId + systemTime;
                String hMerchantCd = merchantCd + systemTime;
                String hVanId     = vanId + systemTime;
                String encryptPartnerId = "";
                String encryptMerchantCd = "";
                String encryptVanId = "";

                try {
                    encryptPartnerId = getH(hPartnerId, "ceappcardswallet");
                    encryptMerchantCd = getH(hMerchantCd, "ceappcardswallet");
                    encryptVanId = getH(hVanId, "ceappcardswallet");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String encryptSHA = getParamHash256(partnerCd + partnerId);


                String uri = "niceappcard://payment?" +
                        "partner_cd=" + partnerCd +
                        "&partner_id=" + encryptPartnerId +
                        "&van_id=" + encryptVanId +
                        "&merchant_cd=" + encryptMerchantCd +
                        "&pay_order=A" +         // App to App 응답 필수값
                        "&payPrice=" + price +   // 5만원 이상 서명 확인
                        "&partner_rgb=FF0000" +  // 제휴사 색상값 변경가능
                        "&h=" + encryptSHA;      // 필수 값
                Log.d("kis", "uri = " + uri);
                intent.setData(Uri.parse(uri));
                startActivityForResult(intent, 0);

            }else{

                AlertDialog.Builder ab = new AlertDialog.Builder(this);
                ab.setTitle("알림");
                ab.setMessage("안전한 결제를 위한 앱 설치가 필요합니다.\n모바일결제 통합모듈을 설치하시겠습니까?");
                ab.setNegativeButton("취소", null);
                ab.setPositiveButton("설치", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uri = Uri.parse("market://search?q=pname:" + APPCARD_PKG);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });
                ab.show();

            }

        }else{

            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.txt_item_not_found_kis_title))
                    .setMessage(getResources().getString(R.string.txt_item_error_price_mix_max))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_pm_qr :

                goToPaymentByCard();

                break;

            case R.id.btn_cancel :

                finish();

                break;

            case R.id.btn_reset :

                input_price.setText(null);

                break;

        }

    }
}
