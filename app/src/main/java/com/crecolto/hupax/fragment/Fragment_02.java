package com.crecolto.hupax.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.crecolto.hupax.R;
import com.crecolto.hupax.utils.AppUtil;

public class Fragment_02 extends Fragment {

    TextView user_name, user_email, user_account_number;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_02, container, false);

        user_name = (TextView) rootView.findViewById(R.id.user_name);
        user_email = (TextView) rootView.findViewById(R.id.user_email);
        user_account_number = (TextView) rootView.findViewById(R.id.user_account_number);

        user_name.setText(String.format(getResources().getString(R.string.item_my_page_name), AppUtil.getUserName()));
        user_email.setText(AppUtil.getUserEmail());
        user_account_number.setText(String.format(getResources().getString(R.string.item_my_page_code), AppUtil.getUserCode()));

        return rootView;

    }
}
