package com.crecolto.hupax.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class AppUtil {

    public static JSONObject userData;

    public static void setUserData(JSONObject jObj){

        userData = jObj;

    }

    public static JSONObject getUserData(){

        return userData;

    }

    public static String getUserCode(){

        String temp = "";

        if(userData != null){

            try {
                temp = userData.getString("user_code");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return temp;

    }

    public static String getUserPoint(){

        String temp = "";

        if(userData != null){

            try {
                temp = userData.getString("user_point");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return temp;

    }

    public static String getUserEmail(){

        String temp = "";

        if(userData != null){

            try {
                temp = userData.getString("user_email");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return temp;

    }

    public static String getUserTeam(){

        String temp = "";

        if(userData != null){

            try {
                temp = userData.getString("user_team");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return temp;

    }

    public static String getUserName(){

        String temp = "";

        if(userData != null){

            try {
                temp = userData.getString("user_name");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return temp;

    }

    public static String strGetter(int type){

        String pm_tv = "";

        switch (type) {

            case 1:

                pm_tv = "교무금";

                break;

            case 3:

                pm_tv = "기타봉헌금";

                break;

            case 7:

                pm_tv = "제단체후원금";

                break;

            case 11:

                pm_tv = "건축헌금";

                break;

            case 12:

                pm_tv = "시설헌금";

                break;

            case 14:

                pm_tv = "기타목적헌금";

                break;

            case 17:

                pm_tv = "선교후원금";

                break;

            case 19:

                pm_tv = "주일헌금";

                break;

            case 20:

                pm_tv = "축일헌금";

                break;

            case 21:

                pm_tv = "성소후원금";

                break;

            case 22:

                pm_tv = "특별헌금";

                break;

            case 34:

                pm_tv = "일반기부금";

                break;

            default:

                pm_tv = "error";

        }

        return pm_tv;

    }

}
