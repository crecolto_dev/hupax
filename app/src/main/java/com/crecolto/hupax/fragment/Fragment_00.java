package com.crecolto.hupax.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.crecolto.hupax.MainActivity;
import com.crecolto.hupax.R;
import com.crecolto.hupax.adapter.MenuPagerAdapter;
import com.crecolto.hupax.utils.AppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import me.relex.circleindicator.CircleIndicator;

public class Fragment_00 extends Fragment {

    ViewPager viewPager;
    MenuPagerAdapter pagerAdapter;
    HashMap<Integer, JSONObject> typeData;
    TextView user_name, user_point, user_team;
    ImageView btn_notification;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_00, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager) ;
        btn_notification = (ImageView) rootView.findViewById(R.id.btn_notification);
        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).movePaymentList();
            }
        });

        user_name = (TextView) rootView.findViewById(R.id.user_name);
        user_point = (TextView) rootView.findViewById(R.id.user_point);
        user_team = (TextView) rootView.findViewById(R.id.user_team);

        user_name.setText(String.format(getResources().getString(R.string.item_dummy_data_01), AppUtil.getUserName()));
        user_point.setText(String.format(getResources().getString(R.string.item_dummy_data_02), AppUtil.getUserPoint()));
        user_team.setText(AppUtil.getUserTeam());

        typeData = new HashMap<>();

        try {
            typeData.put(0, new JSONObject().put("type", 1).put("page", 0));
            typeData.put(1, new JSONObject().put("type", 3).put("page", 0));
            typeData.put(2, new JSONObject().put("type", 7).put("page", 0));
            typeData.put(3, new JSONObject().put("type", 11).put("page", 0));
            typeData.put(4, new JSONObject().put("type", 12).put("page", 1));
            typeData.put(5, new JSONObject().put("type", 14).put("page", 1));
            typeData.put(6, new JSONObject().put("type", 17).put("page", 1));
            typeData.put(7, new JSONObject().put("type", 19).put("page", 1));
            typeData.put(8, new JSONObject().put("type", 20).put("page", 2));
            typeData.put(9, new JSONObject().put("type", 21).put("page", 2));
            typeData.put(10, new JSONObject().put("type", 22).put("page", 2));
            typeData.put(11, new JSONObject().put("type", 34).put("page", 2));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pagerAdapter = new MenuPagerAdapter(getActivity(), typeData) ;
        viewPager.setAdapter(pagerAdapter) ;
        CircleIndicator indicator = (CircleIndicator) rootView.findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

        return rootView;


    }
}
