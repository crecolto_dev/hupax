package com.crecolto.hupax;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.crecolto.hupax.utils.AppUtil;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class SelectPmActivity extends BaseActivity implements View.OnClickListener {

    TextView pm_tv;
    Button btn_pm_card, btn_pm_qr, btn_pm_nfc;
    int type;
    private String otc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_pm);

        pm_tv = (TextView) findViewById(R.id.pm_tv);
        btn_pm_card = (Button) findViewById(R.id.btn_pm_card);
        btn_pm_qr = (Button) findViewById(R.id.btn_pm_qr);
        btn_pm_nfc = (Button) findViewById(R.id.btn_pm_nfc);

        btn_pm_card.setOnClickListener(this);
        btn_pm_qr.setOnClickListener(this);
        btn_pm_nfc.setOnClickListener(this);

        Intent intent = getIntent();
        type = (int) intent.getSerializableExtra("type");

        pm_tv.setText(AppUtil.strGetter(type));

    }

    private void sendPayment(int paymentType){

        /**
         * 0 : credit card
         * 1 : QR code
         * 2 : nfc
         */

        Intent intent = null;

        switch (paymentType){

            case 0 :

                intent = new Intent(SelectPmActivity.this, PaymentActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("payment_type", paymentType);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);break;

            case 1 :

                if(isInstalledApplication(APPCARD_PKG)){

                    IntentIntegrator integrator = new IntentIntegrator(this);
                    integrator.setOrientationLocked(true);
                    integrator.initiateScan();

                }else{

                    AlertDialog.Builder ab = new AlertDialog.Builder(this);
                    ab.setTitle("알림");
                    ab.setMessage("안전한 결제를 위한 앱 설치가 필요합니다.\n모바일결제 통합모듈을 설치하시겠습니까?");
                    ab.setNegativeButton("취소", null);
                    ab.setPositiveButton("설치", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Uri uri = Uri.parse("market://search?q=pname:" + APPCARD_PKG);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    });
                    ab.show();

                }break;

        }

    }

    private final static String APPCARD_PKG = "com.nice.appcard";

    private boolean isInstalledApplication(String pkgName) {

        PackageManager pm = getPackageManager();

        try {
            pm.getApplicationInfo(APPCARD_PKG, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String loadedQrData = "";

    private void moveToPaymentResult(){

        Log.d("move_result", "data : " + loadedQrData);

        String tempStr = loadedQrData.substring(loadedQrData.indexOf("payPrice"));
        String price = tempStr.substring((tempStr.indexOf("=") + 1), tempStr.indexOf("&"));

        Intent intent = new Intent(SelectPmActivity.this, PaymentResultActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("payment_type", 1);
        intent.putExtra("price", Integer.parseInt(price));
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();

    }

    private final String SCAN_LOG = "SCAN_CODE";

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {

        //  com.google.zxing.integration.android.IntentIntegrator.REQUEST_CODE
        //  = 0x0000c0de; // Only use bottom 16 bits

        if (requestCode == IntentIntegrator.REQUEST_CODE) {

            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            if(result != null){

                if(result.getContents() == null) {

                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();

                } else {

                    Log.d(SCAN_LOG, result.getContents());
                    loadedQrData = result.getContents();
                    openApp(result.getContents());
                }

            }else{

                super.onActivityResult(requestCode, resultCode, data);

            }

        } else if(requestCode == 0){

            Log.d(SCAN_LOG, "request code 0");

            if(resultCode == RESULT_OK) {

                Log.i(SCAN_LOG, "resultType : " + data.getStringExtra("resultType"));
                Log.i(SCAN_LOG, "resultCode : " + resultCode);
                Log.i(SCAN_LOG, "result all data : " + data.toUri(Intent.URI_INTENT_SCHEME));
                Log.i(SCAN_LOG, "resultOTC : " + data.getStringExtra("OTC"));
                Log.i(SCAN_LOG, "resultID_CD : " + data.getStringExtra("ID_CD"));

                moveToPaymentResult();

            }else{

                rsDialog(resultCode);

            }

        }else {

            super.onActivityResult(requestCode, resultCode, data);

        }

    }

    private void rsDialog(int resultCode){

        String rsStr = "";

        switch (resultCode){

            case 0 : rsStr = getResources().getString(R.string.item_payment_appcard_0);break;

            case 400 : rsStr = getResources().getString(R.string.item_payment_appcard_400);break;

            case 405 : rsStr = getResources().getString(R.string.item_payment_appcard_405);break;

            case 801 : rsStr = getResources().getString(R.string.item_payment_appcard_801);break;

            case 802 : rsStr = getResources().getString(R.string.item_payment_appcard_802);break;

            case 803 : rsStr = getResources().getString(R.string.item_payment_appcard_803);break;

            case 812 : rsStr = getResources().getString(R.string.item_payment_appcard_812);break;

            case 813 : rsStr = getResources().getString(R.string.item_payment_appcard_813);break;

            case 815 : rsStr = getResources().getString(R.string.item_payment_appcard_815);break;

            case 201 : rsStr = getResources().getString(R.string.item_payment_appcard_201);break;

            case 202 : rsStr = getResources().getString(R.string.item_payment_appcard_202);break;

            default :  rsStr = getResources().getString(R.string.item_payment_appcard_default);

        }

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.item_payment_appcard_title))
                .setMessage(rsStr)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }

    private void openApp(String resultData){

        Log.d(SCAN_LOG, "intent data : " + resultData);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(resultData));
        startActivityForResult(intent, 0);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_pm_card : sendPayment(0);break;

            case R.id.btn_pm_qr : sendPayment(1); break;

            case R.id.btn_pm_nfc : sendPayment(2);break;

        }

    }
}
