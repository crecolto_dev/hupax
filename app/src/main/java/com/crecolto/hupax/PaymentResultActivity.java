package com.crecolto.hupax;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.crecolto.hupax.utils.AppUtil;

public class PaymentResultActivity extends BaseActivity implements View.OnClickListener {

    private int type, p_type, price;
    private TextView pm_tv, tv_price, tv_type, user_name, user_name_01, tv_user_team;
    private Button btn_ok;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_result);

        Intent intent = getIntent();
        type = (int) intent.getSerializableExtra("type");
        p_type = (int) intent.getSerializableExtra("payment_type");
        price = (int) intent.getSerializableExtra("price");

        pm_tv = (TextView) findViewById(R.id.pm_tv);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(this);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_type = (TextView) findViewById(R.id.tv_type);
        user_name = (TextView) findViewById(R.id.user_name);
        tv_user_team = (TextView) findViewById(R.id.tv_user_team);
        user_name_01 = (TextView) findViewById(R.id.user_name_01);

        pm_tv.setText(AppUtil.strGetter(type));
        tv_type.setText(" " + setElement(p_type));
        tv_price.setText(" " + String.format(getResources().getString(R.string.item_result_price_won), String.valueOf(price)));
        user_name.setText(String.format(getResources().getString(R.string.item_user_name), AppUtil.getUserName()));
        user_name_01.setText(String.format(getResources().getString(R.string.item_result_thanks), AppUtil.getUserName()));
        tv_user_team.setText(AppUtil.getUserTeam());
        Log.d("set", "ok");

    }

    private String setElement(int p_type){

        /**
         * p_type
         * 0 : credit card
         * 1 : QR code
         * 2 : nfc
         */

        String str = "";

        switch (p_type){

            case 0 : str = getResources().getString(R.string.item_pm_kind_00);break;

            case 1 : str = getResources().getString(R.string.item_pm_kind_01);break;

            case 2 : str = getResources().getString(R.string.item_pm_kind_02);break;

        }

        return str;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_ok : finish();break;

        }

    }
}
