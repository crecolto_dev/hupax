package com.crecolto.hupax;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.crecolto.hupax.fragment.Fragment_00;
import com.crecolto.hupax.fragment.Fragment_01;
import com.crecolto.hupax.fragment.Fragment_02;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout btn_tab_00, btn_tab_01, btn_tab_02;
    private FragmentManager fragmentManager;
    private Fragment_00 fragment_00;
    private Fragment_01 fragment_01;
    private Fragment_02 fragment_02;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_tab_00 = (LinearLayout) findViewById(R.id.btn_tab_00);
        btn_tab_01 = (LinearLayout) findViewById(R.id.btn_tab_01);
        btn_tab_02 = (LinearLayout) findViewById(R.id.btn_tab_02);

        btn_tab_00.setOnClickListener(this);
        btn_tab_01.setOnClickListener(this);
        btn_tab_02.setOnClickListener(this);

        fragmentManager = getSupportFragmentManager();

        fragment_00 = new Fragment_00();
        fragment_01 = new Fragment_01();
        fragment_02 = new Fragment_02();

        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fl_content, fragment_00).commitAllowingStateLoss();

    }

    public void movePM(int type){

        Intent intent = new Intent(MainActivity.this, SelectPmActivity.class);
        intent.putExtra("type", type);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

    }

    public void movePaymentList(){

        Intent intent = new Intent(MainActivity.this, PaymentListActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

    }

    private final long FINISH_INTERVAL_TIME = 2000;
    private long backPressedTime = 0;

    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime){

            super.onBackPressed();

        }else{
            backPressedTime = tempTime;
            toastMaker(getResources().getString(R.string.item_txt_exit));
        }
    }

    @Override
    public void onClick(View v) {

        transaction = fragmentManager.beginTransaction();

        switch (v.getId()){

            case R.id.btn_tab_00 :

                transaction.replace(R.id.fl_content, fragment_00).commitAllowingStateLoss();

                break;

            case R.id.btn_tab_01 :

                transaction.replace(R.id.fl_content, fragment_01).commitAllowingStateLoss();

                break;

            case R.id.btn_tab_02 :

                transaction.replace(R.id.fl_content, fragment_02).commitAllowingStateLoss();

                break;

        }

    }
}
