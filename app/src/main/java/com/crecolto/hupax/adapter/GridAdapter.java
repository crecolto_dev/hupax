package com.crecolto.hupax.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crecolto.hupax.MainActivity;
import com.crecolto.hupax.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GridAdapter extends BaseAdapter {

    Context context;
    HashMap<Integer, JSONObject> values;
    LayoutInflater inflater;

    public GridAdapter(Context context, HashMap<Integer, JSONObject> main){

        this.context = context;
        this.values = main;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView title;
        ImageView image;

        if(convertView == null){

            convertView = inflater.inflate(R.layout.item_grid,parent,false);

        }

        image = (ImageView)convertView.findViewById(R.id.grid_iv);
        title = (TextView)convertView.findViewById(R.id.grid_tv);

        try {

            int type = values.get(position).getInt("type");

            switch (type){

                case 1 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("교무금");

                    break;

                case 3 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("기타봉헌금");

                    break;

                case 7 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("제단체후원금");

                    break;

                case 11 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("건축헌금");

                    break;

                case 12 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("시설헌금");

                    break;

                case 14 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("기타목적헌금");

                    break;

                case 17 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("선교후원금");

                    break;

                case 19 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("주일헌금");

                    break;

                case 20 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("축일헌금");

                    break;

                case 21 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("성소후원금");

                    break;

                case 22 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bible));
                    title.setText("특별헌금");

                    break;

                case 34 :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.bird));
                    title.setText("일반기부금");

                    break;

                default :

                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.cross));
                    title.setText("error");

            }

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("CURRENT_DATA", "DATA : " + values.get(position).toString());

                    if (context instanceof MainActivity) {
                        try {
                            ((MainActivity)context).movePM(values.get(position).getInt("type"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder{
        public static ImageView image;
        public static TextView title;
    }
}
