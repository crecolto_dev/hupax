package com.crecolto.hupax;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.crecolto.hupax.utils.AppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class SplashActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout uiLogo, uilogin;
    EditText input_id, input_pw;
    Button btnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        uiLogo = (LinearLayout) findViewById(R.id.ui_comp_logo);
        uilogin = (LinearLayout) findViewById(R.id.ui_comp_login);
        btnLogin = (Button) findViewById(R.id.btn_login);
        input_id = (EditText) findViewById(R.id.input_id);
        input_pw = (EditText) findViewById(R.id.input_pw);
        btnLogin.setOnClickListener(this);

        try {

            JSONObject userData = new JSONObject(loadUserData());

            if(userData != null){

                toastMaker("USER DATA LOADED");

                AppUtil.setUserData(userData.getJSONObject("data"));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        isLogin(0);

                    }
                }, 2000);

            }else{

                toastMaker("USER DATA LOAD FAIL");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private String loadUserData() throws IOException {

        InputStream is = getResources().openRawResource(R.raw.user_dummy_data);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];

        try {

            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;

            while ((n = reader.read(buffer)) != -1){

                writer.write(buffer, 0, n);

            }

        } finally {

            is.close();

        }

        String jStr = writer.toString();

        return jStr;

    }

    private void isLogin(int resultValue){

        /**
         * 0 : not login
         * 1 : login ok
         */

        switch (resultValue){

            case 0:

                if(uiLogo.getVisibility() != View.GONE){

                    uiLogo.setVisibility(View.GONE);
                    uilogin.setVisibility(View.VISIBLE);

                }break;

            case 1: afterCheckAccount(true);break;

            case 99:

                uiLogo.setVisibility(View.GONE);
                uilogin.setVisibility(View.GONE);break;

        }

    }

    private void afterCheckAccount(boolean tf){

        if(tf){

            hideProgress();
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            finish();

        }else{



        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_login :

                if(input_id.getText().toString().equals("") || input_id.getText().toString().length() == 0 || input_pw.getText().toString().equals("") || input_pw.getText().toString().length() == 0){

                    toastMaker(getResources().getString(R.string.alert_id_and_pw));
                    break;

                }

                showProgress();
                isLogin(99);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        afterCheckAccount(true);

                    }
                }, 2000);
                break;

        }

    }
}
