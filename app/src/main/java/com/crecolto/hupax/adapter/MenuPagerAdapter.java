package com.crecolto.hupax.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.crecolto.hupax.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MenuPagerAdapter extends PagerAdapter {

    private Context mContext = null ;
    GridView gridView;
    HashMap<Integer, JSONObject> typeData;
    GridAdapter gAdapter;

    public MenuPagerAdapter() {

    }

    public MenuPagerAdapter(Context context, HashMap <Integer, JSONObject> tempData) {
        mContext = context ;
        typeData = tempData;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = null ;

        if (mContext != null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.ui_pager, container, false);

            gridView = (GridView) view.findViewById(R.id.gridView);

            HashMap<Integer, JSONObject> tempData = new HashMap<>();

            int count = 0;

            for(int i = 0; i < typeData.size(); i++){

                try {

                    if(typeData.get(i).getInt("page") == position){

                        JSONObject temp = typeData.get(i);
                        tempData.put(count, temp);
                        count++;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            gAdapter = new GridAdapter(mContext, tempData);
            gridView.setAdapter(gAdapter);

        }

        container.addView(view) ;

        return view ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (View)object);
    }
}
